import os
from guardian import GuardState

ca_prefix = 'TEST'

class INIT(GuardState):
    def main(self):
        ezca['A'] = ezca['B'] = ezca['C'] = ezca['D'] = 0
        log('FOO')

class A(GuardState):
    goto = True
    def main(self):
        ezca['A'] = 1

class B(GuardState):
    def main(self):
        ezca['A'] = 0
        ezca['B'] = 1

class C(GuardState):
    def main(self):
        ezca['C'] = 1

    def run(self):
        if ezca['C'] == 1:
            return False
        ezca['C'] = 2
        return True

class D(GuardState):
    redirect = False
    def main(self):
        ezca['C'] = 1

    def run(self):
        if ezca['C'] == 1:
            return False
        ezca['C'] = 2
        return True

class E(GuardState):
    def run(self):
        ezca['D'] = 3

class F(GuardState):
    def main(self):
        ezca['C'] = 1

    def run(self):
        if ezca['C'] == 1:
            return False
        ezca['C'] = 2
        return True

edges = [
    ('INIT', 'A'),
    ('A', 'B'),
    ('B', 'C'),
    ('B', 'D'),
    ('D', 'E'),
    ('B', 'F'),
    ('F', 'F'),
    ]
