from guardian import GuardState

from TEST0 import *

request = 'A'
nominal = 'D'

prefix = 'TEST-'

class D(GuardState):
    index = 100
    def run(self):
        ezca['D'] = -1
        return True

edges += [
    ('C', 'D'),
    ]
