# declare prerequisites for external binaries used in tests
# test_declare_external_prereq python

if [ -z "$TEST_DIRECTORY" ] ; then
    TEST_DIRECTORY=$(cd $(dirname $0)/.. && pwd)
fi

export SRC_DIRECTORY=$(cd "$TEST_DIRECTORY"/.. && pwd)
export TEST_BIN=$(cd "$TEST_DIRECTORY"/bin && pwd)
export PATH="$TEST_BIN":"$SRC_DIRECTORY"/bin:$PATH
export PYTHONPATH="$SRC_DIRECTORY"/lib:$PYTHONPATH
export PYTHON=${PYTHON:-python3}

export EPICS_CA_ADDR_LIST='localhost:58900 localhost:58901 localhost:58902 localhost:58903 localhost:58904'
export EPICS_CA_AUTO_ADDR_LIST=NO

export SITE="LTO"
export IFO="T1"
export GUARD_MODULE_PATH=$(readlink -f "$TEST_DIRECTORY/modules")
export GUARD_LOG_LEVEL=${GUARD_LOG_LEVEL:=DEBUG}
export EZCA_TIMEOUT=1

################################################################

guardutil() {
    exec $PYTHON -u -m guardian.guardutil "$@"
}
export guardutil

wait_node_ready() {
    local chan="${IFO}:GRD-${1}_STATUS"
    until caget "$chan" &>/dev/null ; do
	sleep .1
    done
}

cawait() {
    local channel="$1"
    local value="$2"
    local timeout=20
    local start=$(date +%s)
    local stop=$((start + timeout))
    local now
    until [[ $(caget -t "$channel") == "$value" ]] ; do
	now=$(date +%s)
	if ((now > stop)) ; then
	    return 1
	fi
	sleep .1
    done
}

node_dump_state() {
    local node="${IFO}:GRD-${1}"
    caget -F. \
	"$node"_STATE{,_N,_S} \
	"$node"_TARGET{,_N,_S} \
	"$node"_REQUEST{,_N,_S} \
	"$node"_NOMINAL{,_N,_S} \
	"$node"_OP \
	"$node"_MODE \
	"$node"_STATUS \
	"$node"_ERROR \
	"$node"_MANAGER \
	"$node"_STALLED \
        "$node"_ACTIVE \
        "$node"_SUBNODES_TOTAL \
        "$node"_SUBNODES_NOT_OK \
        "$node"_READY \
        "$node"_INTENT \
	"$node"_OK \
	| sort
}

node_git_hex() {
    $PYTHON -c "print(hex($1)[2:].zfill(7))"
}

kill_jobs() {
    kill `jobs -p`
    i=0
    while [ "$(jobs -p)" ] && ((i<10)) ; do
        sleep 0.1
        ((i+=1))
    done
    if [ "$(jobs -p)" ] ; then
        kill -9 `jobs -p`
        wait
    fi
}
