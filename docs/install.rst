Installation
============

The guardian source is available from the main guardian svn:

 * https://redoubt.ligo-wa.caltech.edu/svn/guardian

The code base consists of `guardian` python library, the main
:ref:`guardian <daemon>` daemon program, and various utility and
helper apps.

external library dependencies
-----------------------------

Guardian depends on a couple of other packages.  Only the ``cdsutils``
package is needed for executing single states from the command line.
The guardian state machine engine daemon additionally needs the pcaspy
and networkx libraries.  pydot is needed for drawing graphs.

The following packages are needed for running Guardian.  The
*Dependencies* are packages that are required for essentially any
usage of Guardian.  The *Recommends* are packages that significantly
increase the usability, but are not strictly required for all use
cases.  Python module/package names are followed (in parentheses) by
the OS-level package names available on most \*NIX systems.

Dependencies:

 * `Python 2.7 <https://www.python.org/download/releases/2.7/>`_
   (``python2.7``):
 * :py:mod:`networkx` (``python-networkx``): `NetworkX networking
   library <http://networkx.github.io/>`_, used for internal
   representations of the state graph object, and for calculating
   paths on the state graphs.

Recommends:

 * :py:mod:`pyepics` (``python-pyepics``): `Python EPICS channel
   access client interface
   <http://cars.uchicago.edu/software/python/pyepics/index.html>`_.
 * :py:mod:`pcaspy` (``python-pcaspy``): `Python EPICS portable
   channel access server
   <https://pcaspy.readthedocs.org/en/latest/>`_.
   Used for the :program:`guardian` daemon control interface.
 * :py:mod:`pydot` (``python-pydot``): Python DOT graphing interfce,
   for drawing system state graphs with :program:`guardutil`.
 * :py:mod:`prctl` (``python-prctl``): process control interface.
 * :py:mod:`git` (``python-git``): `git <https://git-scm.com/>`_ SCM
   interface.  Used for user code archiving.

.. warning:: **All of the packages listed above are required for the
             aLIGO system installs.**
  

:py:mod:`cdsutils` and :py:mod:`ezca`
-------------------------------------

Guardian modules interact with the aLIGO CDS system via `EPICS
<http://www.aps.anl.gov/epics/>`_.  In particular, Guardian uses the
:ref:`ezca` interface, which is a wrapping of the `pyepics
<http://cars.uchicago.edu/software/python/pyepics/index.html>`_
interface specifically designed for interaction between Guardian and
the aLIGO RTS system.  :ref:`Ezca <ezca>` is provided as part of the
``cdsutils`` package.

.library, which is specifically
designed for interacting with the LIGO CDS system and for use in
Guardian.  It is available in the [[CDS
Software/ControlRoomTools|cdsutils]] package.  See the following page
for how to install the cdsutils package:

 * [[CDS Software/ControlRoomTools/installation]]

pcaspy
------

The :ref:`guardian <daemon>` state machine engine daemon uses the
[[https://pypi.python.org/pypi/pcaspy|python EPICS portable channel
access server (pcaspy)]] for creating EPICS control and status
channels.  This package can either be
[[http://pythonhosted.org/pcaspy/installation.html|installed from
source]], or Jamie has built debian packages that can be retrieved
from his personal git repository of the pcaspy source:

 * git://finestructure.net/pcaspy

To build the package from a checkout of Jamie's source, do the
following::

    $ git clone git://finestructure.net/pcaspy
    $ cd pcaspy
    $ EPICS_BASE=/ligo/apps/linux-x86_64/epics/base ./debsnap
    $ sudo dpkg -i build/python-pcaspy_0.4.1-1_amd64.deb

This requires the package: swig.



python-git
^^^^^^^^^^
 
The guardian usercode archiving feature (>r1390) requires the
python-git package.  Unfortunately it requires versions >0.3, which
are newer than what's available in Ubuntu 12.04 (0.1.6-1).
Fortunately, the newer version can be fairly easily retrieved from
Ubuntu 14.04::

    root@l1script0:~# cat /etc/apt/sources.list.d/trusty.list 
    deb http://us.archive.ubuntu.com/ubuntu/ trusty universe
    deb-src http://us.archive.ubuntu.com/ubuntu/ trusty universe
    root@l1script0:~# cat /etc/apt/preferences.d/pinning 
    Package: *
    Pin: release n=trusty
    Pin-Priority: -10
    root@l1script0:~# apt-get install python-git/trusty python-gitdb/trusty python-async/trusty python-smmap/trusty


.. _install_sec:

Installation
------------

Hopefully there will soon be proper releases.  Until then, there are
three ways to install the code base from the SVN:

.. note:: the methods that install directly from SVN can produce
	  unreliable version numbers in the packaging and code.  Care
	  should be taken, and they should probably not be used in
	  "production" scenarios.

`ligo-install`
^^^^^^^^^^^^^^

The `ligo-install` make target installs the code directly from source
into a version-specific sub-directory under the directory specified by
the :envvar:`APPSROOT` environment variable.  If not specified,
:envvar:`APPSROOT` defaults to the "standard" LIGO CDS NFS software
directory, e.g.::

    APPSROOT=/ligo/apps/linux-x86_64

Usually you would run "make" as a normal user, to update the version
number and build the byte-compiled binaries, the run "make
ligo-install" as the ``controls`` user to install it to
``/ligo/apps/linux-x86-64/guardian-$(VERSION)``::
  
    $ make
    $ su controls -c "make ligo-install"

In order to make this install usable the installed configuration file
must be sourced into your bash shell, either manually or in a bashrc
file:

    $ . $APPSROOT/etc/guardian-user-env.sh

NOTE: This method is meant to be a ''temporary'' stop-gap measure
until we have more stable releases, in which case the use of proper
packaging is preferred.


deb-snapshot
^^^^^^^^^^^^

Debian/Ubuntu snapshot packages can be built with the 'deb-snapshot'
target.  This requires the Debian package building utilities,
e.g. build-essential, as well as the guardian build dependencies:

 * build-essential
 * pkg-config
 * python-all-dev
 * python-setuptools
 * debhelper

Notes for older distributions (Squeese, Ubuntu 10.04):

 * debhelper version 9 is available from squeeze backports
   ("apt-get -t squeeze-backports install debhelper")
 * python-argparse (this is needed for python2.6 or earlier, but is
   included by default with python2.7)

::

   $ make deb-snapshot
   ...
   $ ls build/
    ...
    build/guardian_0.1-1_amd64.deb
    ...
    $
