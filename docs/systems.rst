.. _systems:

Guardian `Systems`
==================

system descriptions modules
---------------------------

Guardian :ref:`systems` are defined by simple python `modules
<https://docs.python.org/2/tutorial/modules.html>`_, which is a file
with the ``.py`` extension.  The module should be named after the
system, so for instance the system `ISC_LOCK` would have the name
``ISC_LOCK.py``.

.. note:: By convention all `system` names are upper case, with words
	  separated by underscores.

The system module describes the state graph by defining states in the
form of :class:`~guardian.GuardState` classes named for the state
name, and connections between the states via an :ref:`edges` list::

  from guardian import GuardState

  class SAFE(GuardState):
      ...

  class DAMPED(GuardState):
      ...

  edges = [
      ('SAFE', 'DAMPED'),
  ]

When guardian loads the system module it constructs the :ref:`state
graph <state graph>`, which becomes it's internal representation of the
automation logic.


state definitions
^^^^^^^^^^^^^^^^^

States are class definitions that inherit from the
:class:`~guardian.GuardState` base class.  The name of the class is
the name of the corresponding state.

.. note:: By convention all `state` names are upper case, with words
	  separated by underscores.

The :class:`~guardian.GuardState` class has two methods that are
overridden to program the state behavior::

  class DAMPED(GuardState):

      # main method executed once
      def main(self):
          ...

      # run method executed in a loop
      def run(self):
          ...

The :py:meth:`main` and :py:meth:`run` methods can execute *arbitrary
python code*.


.. _edges:

edge definitions
^^^^^^^^^^^^^^^^

`Edges` are content-less connections between states that indicate
which transitions between starts are allowable in the system.

State graphs are `directed`, which means that edges have an
orientation that points from one state to another.  An edge definition
is therefore :py:obj:`tuple` with the first element being the "from"
state, and the second element being the "to" state::

  edge0 = ('STATE0', 'STATE1')

Ultimately, guardian is looking for an system module attribute called
:ref:`edges`, which is a simple list of edge definitions::

  edges = [
      edge0,
      ('STATE1', 'STATE2'),
  ]

States may also be specified as :py:attr:`~guardian.GuardState.goto`,
which will cause guardian to automatically add edges to this state
from all other states::

  class SAFE(GuardState):
    goto = True

(Guardian also treats "goto" states differently.  See the
section on :ref:`redirection` for more info.


.. _state graph:

The system state graph
----------------------	  

Whe :program:`guardian` loads the system module, it parses the module
to find all :class:`~guardian.GuardState` class definitions, as well
as the :ref:`edges` definition.  It then

.. figure:: _static/graph-example.png
   :width: 200px
   :figwidth: 600px

   State graph.


.. _path calc:

Path calculations
-----------------


.. _redirection:

"Goto" states and redirection
-----------------------------
