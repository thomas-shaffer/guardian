import os

# needed variables
ENV = dict(
    IFO=None,
    GUARD_CHANFILE=None,
)

# load from conf file
CONFFILE = os.getenv('GUARD_CONFFILE', '/etc/guardian/local-env')
if os.path.exists(CONFFILE):
    with open(CONFFILE, 'r') as f:
        for line in f:
            if not line.strip():
                continue
            if line.strip()[0] == '#':
                continue
            try:
                name, value = line.strip().split('=', 1)
            except ValueError:
                raise SystemExit("invalid variable definition: {}".format(line))
            if name != name.upper():
                raise SystemExit("invalid variable name (not upper): {}".format(name))
            if name in ENV:
                ENV[name] = value

# load from environment
for name, value in list(ENV.items()):
    ENV[name] = os.getenv(name, value)
