color_palette = {
    'white':            'ffffff',
    'black':            '000000',
    'state_normal':     'b3cde3',
    'state_normal_req': '377eb8',
    'state_goto':       'ccebc5',
    'state_goto_req':   '4daf4a',
    'state_init':       'decbe4',
    'state_init_req':   '984ea3',
    'state_none':       '919191',
    }

medm_default_color_list = [
    'ffffff',
    'ececec',
    'dadada',
    'c8c8c8',
    'bbbbbb',
    'aeaeae',
    '9e9e9e',
    '919191',
    '858585',
    '787878',
    '696969',
    '5a5a5a',
    '464646',
    '2d2d2d',
    '000000',
    '00d800',
    '1ebb00',
    '339900',
    '2d7f00',
    '216c00',
    'fd0000',
    'de1309',
    'be190b',
    'a01207',
    '820400',
    '5893ff',
    '597ee1',
    '4b6ec7',
    '3a5eab',
    '27548d',
    'fbf34a',
    'f9da3c',
    'eeb62b',
    'e19015',
    'cd6100',
    'ffb0ff',
    'd67fe2',
    'ae4ebc',
    '8b1a96',
    '610a75',
    'a4aaff',
    '8793e2',
    '6a73c1',
    '4d52a4',
    '343386',
    'c7bb6d',
    'b79d5c',
    'a47e3c',
    '7d5627',
    '58340f',
    '99ffff',
    '73dfff',
    '4ea5f9',
    '2a63e4',
    '0a00b8',
    'ebf1b5',
    'd4db9d',
    'bbc187',
    'a6a462',
    '8b8239',
    '73ff6b',
    '52da3b',
    '3cb420',
    '289315',
    '1a7309',
]

grd_color_list = [
    'ffffff',
    '000000',
    'c8c8c8',
    'bbbbbb',
    '858585',
    '464646',
    '339900',
    'fd0000',
    '5893ff',
    '597ee1',
    '4b6ec7',
    '3a5eab',
    '27548d',
    'fbf34a',
    'cd6100',
    'ffb0ff',
    '8b1a96',
    '4ea5f9',
]

# import sys
# print color_list[int(sys.argv[1])]

#print len(color_list), color_list[-1]
color_list = medm_default_color_list[:-len(color_palette)-1]
for i, value in enumerate(color_palette.values()):
    ii = i+1
    if value not in color_list:
        color_list.append(value)
#print len(color_list), color_list[-1]

def crslv(name):
    return color_list.index(color_palette[name])

##################################################

def header(**kwargs):
    if 'bclr' not in kwargs:
        kwargs['bclr'] = 29
    ss = '''
file {{
	name="GRD_AUTOGEN.adl"
	version=030107
}}
display {{
	object {{
		x=0
		y=0
		width={width}
		height={height}
	}}
	clr=14
	bclr={bclr}
	cmap=""
	gridSpacing=5
	gridOn=0
	snapToGrid=0
}}
'''.format(**kwargs)
    ss += '''
"color map" {{
	ncolors=65
	colors {{
'''
    for color in color_list:
        ss += '''
               {0},'''.format(color)
    ss += '''
}}
'''
    return ss

def background(**kwargs):
    return '''
rectangle {{
	object {{
		x=0
		y=0
		width={width}
		height={height}
	}}
	"basic attribute" {{
		clr=29
	}}
}}
rectangle {{
	object {{
		x=0
		y=0
		width={width}
		height={height}
	}}
	"basic attribute" {{
		clr=39
	}}
	"dynamic attribute" {{
		vis="calc"
		calc="A=1"
		chan="$(IFO):GRD-$(SYSTEM)_MODE"
	}}
}}
rectangle {{
	object {{
		x=0
		y=0
		width={width}
		height={height}
	}}
	"basic attribute" {{
		clr=35
	}}
	"dynamic attribute" {{
		vis="calc"
		calc="A=2"
		chan="$(IFO):GRD-$(SYSTEM)_MODE"
	}}
}}
rectangle {{
	object {{
		x=0
		y=0
		width={width}
		height={height}
	}}
	"basic attribute" {{
		clr=24
	}}
	"dynamic attribute" {{
		vis="if not zero"
		chan="$(IFO):GRD-$(SYSTEM)_CONNECT"
	}}
}}
rectangle {{
	object {{
		x=0
		y=0
		width={width}
		height={height}
	}}
	"basic attribute" {{
		clr=30
	}}
	"dynamic attribute" {{
		vis="calc"
		calc="A=1"
		chan="$(IFO):GRD-$(SYSTEM)_OP"
	}}
}}
rectangle {{
	object {{
		x=0
		y=0
		width={width}
		height={height}
	}}
	"basic attribute" {{
		clr=20
	}}
	"dynamic attribute" {{
		vis="if not zero"
		chan="$(IFO):GRD-$(SYSTEM)_ERROR"
	}}
}}
rectangle {{
	object {{
		x=0
		y=0
		width={width}
		height=18
	}}
	"basic attribute" {{
		clr=29
	}}
	"dynamic attribute" {{
		chan="BLAH"
	}}
}}
polyline {{
	object {{
		x=0
		y=17
		width={width}
		height=1
	}}
	"basic attribute" {{
		clr=25
		fill="outline"
		width=1
	}}
	"dynamic attribute" {{
		chan="BLAH"
	}}
	points {{
		(0,17)
		({width},17)
	}}
}}
'''.format(**kwargs)

def title(x):
    return '''
text {{
	object {{
		x={x}
		y=2
		width=100
		height=12
	}}
	"basic attribute" {{
		clr=4
	}}
	"dynamic attribute" {{
		chan="BLAH"
	}}
	textix="GUARDIAN:"
	align="horiz. right"
}}
text {{
	object {{
		x={x1}
		y=2
		width=100
		height=12
	}}
	"basic attribute" {{
		clr=0
	}}
	"dynamic attribute" {{
		chan="BLAH"
	}}
	textix="$(SYSTEM)"
	align="horiz. left"
}}
'''.format(x=x, x1=x+102)

##################################################

def related_display_menu(displays, **kwargs):
    ss = '''
"related display" {{
       object {{
               x={x}
               y={y}
               width={width}
               height={height}
       }}'''.format(**kwargs)
    for i, display in enumerate(displays):
        ss +='''
       display[{0}] {{
               label="{1}"
               name="{2}"
               args="{3}"
       }}'''.format(i, display[0], display[1], display[2])
    ss += '''
       clr=0
       bclr=12
       label="related"
}}
'''
    return ss

##################################################

def state_request_button(**kwargs):
    if 'width' not in kwargs:
        kwargs['width'] = 270

    kwargs['xe'] = kwargs['x']
    kwargs['widthe'] = 18
    kwargs['xb'] = kwargs['x'] + kwargs['widthe']
    kwargs['widthb'] = kwargs['width'] - kwargs['widthe']

    state = kwargs['state']
    is_goto = kwargs['is_goto']
    is_request = kwargs['is_request']

    clr = crslv('black')
    bclr = crslv('state_normal')

    if state == 'NONE':
        bclr = crslv('state_none')
    elif state == 'INIT':
        if is_request:
            clr = crslv('white')
            bclr = crslv('state_init_req')
        else:
            bclr = crslv('state_init')
    elif is_goto:
        if is_request:
            clr = crslv('white')
            bclr = crslv('state_goto_req')
        else:
            bclr = crslv('state_goto')
    else:
        if is_request:
            clr = crslv('white')
            bclr = crslv('state_normal_req')
    kwargs['clr'] = clr
    kwargs['bclr'] = bclr

    return '''
"shell command" {{
	object {{
		x={xe}
		y={y}
		width={widthe}
		height=26
	}}
	command[0] {{
		label="edit"
		name="guardutil"
		args="edit $(SYSTEM) {state} &"
	}}
	clr=0
	bclr=12
	label="-e"
}}
"message button" {{
	object {{
		x={xb}
		y={y}
		width={widthb}
		height=26
	}}
	control {{
		chan="$(IFO):GRD-$(SYSTEM)_REQUEST"
		clr={clr}
		bclr={bclr}
	}}
	label="{state}  [{index}]"
        release_msg="{state}"
}}
'''.format(**kwargs)
