import os

def get_path(screen):
    return os.path.join(os.path.dirname(__file__), screen)
