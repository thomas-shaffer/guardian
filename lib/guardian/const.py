import os
import socket

# SITE and IFO designators
SITE = os.getenv('SITE', '')
IFO = os.getenv('IFO', '')

# system hostname
HOSTNAME = socket.gethostname()

# daemon EPICS channel access server channel prefix
CAS_PREFIX_FMT = '{IFO}:GRD-{SYSTEM}_'

# main daemon step time in Hz
CPS = int(os.getenv('GUARD_CPS', 16))
if 1000000 % CPS != 0:
    raise ValueError("CPS does not correspond to an integer number of microseconds")

# how long to wait, in seconds, on REDIRECT before terminating worker
REDIRECT_TIMEOUT = 1

# channel names length
CHANNEL_NAME_LENGTH = 60

# USERMSG char array length
USERMSG_STRING_LENGTH = 100

# number of USERMSG channels
USERMSG_COUNT = 10

# how frequently to step through USERMSG
USERMSG_CYCLE_TIME = 2

# number of SPM_DIFF channels
SPM_DIFF_COUNT = 10

# maximum acceptable length of state names.
# (39 characters is the EPICS string length limit)
STATE_NAME_LENGTH = 39

# list of reserved state names and indices
RESERVED_STATES = [
    ('NONE', -1),
    ('????', -2),
    ('????', -3),
    ('????', -4),
    ('????', -5),
    ('????', -6),
    ('????', -7),
    ('????', -8),
    ('????', -9),
]

# default weight of goto edges
DEFAULT_GOTO_EDGE_WEIGHT = 1

# number of allowable REQUEST states
# HACK: EPICS allows for only 16 element enums, and the NONE state is
# always added, so 15 is the limit
REQUEST_STATE_LIMIT = 15
