import os
import subprocess


def xclip(text, isfile=False):
    """Copy text or file contents into X clipboard."""
    f = None
    if isfile:
        f = open(text, 'r')
        sin = f
    else:
        sin = subprocess.PIPE
    p = subprocess.Popen(["xclip", "-i"],
                         stdin=sin)
    p.communicate(text)
    if f:
        f.close()


def background_cmd(ui, cmd):
    #subprocess.call(cmd)
    subprocess.Popen(cmd)
    return
    #FD = open(os.devnull, 'wb')
    stdin = open('/dev/null', 'rb')
    stdout = open('/dev/null', 'wb')
    stderr = open('FOO', 'wb')
    #FD = open('/dev/null', 'rwb')
    subprocess.call(cmd,
                    stdin=stdin,
                    stdout=stdout,
                    stderr=foo, #stdout, #subprocess.STDOUT, #ui.devnull)
                    close_fds=True,
                    )
    # p = subprocess.Popen(cmd,
    #                      stdin=stdin,
    #                      stdout=ui.log, #subprocess.PIPE,
    #                      stderr=subprocess.STDOUT,
    #                      )
    # p.communicate()


def view_file(self, doc):
    """open document file"""
    path = doc.get_fullpaths()
    if not path:
        self.ui.set_status('No file for document id:%d.' % entry.docid)
        return
    path = path[0]
    if not os.path.exists(path):
        self.ui.set_status('ERROR: id:%d: file not found.' % entry.docid)
        return
    #self.ui.set_status('opening file: %s...' % path)
    subprocess.Popen(['xdg-open', path],
                     stdin=self.ui.devnull,
                     stdout=self.ui.devnull,
                     stderr=self.ui.devnull)
