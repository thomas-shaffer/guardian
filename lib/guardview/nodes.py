import os
import sys
import urwid
import functools
import threading
import collections

import epics

import logging
#logging.basicConfig(filename='guardctrl.log', level=logging.DEBUG)

from . import util
from . import palette

############################################################

NODE_ATTRS = collections.OrderedDict([
    ('NODE', (15, 'NODE', 'left')),
    ('VERSION_S', (7, 'VERS', 'right')),
    ('ARCHIVE_ID', (9, 'ARCV ID', 'right')),
    ('OP', (5, 'OP', 'left')),
    ('MODE', (7, 'MODE', 'left')),
    ('STALLED', (7, 'STALLED', 'left')),
    # ('EXECTIME', (9, 'EXECTIME', 'right')),
    ('ERROR', (1, 'E', 'left')),
    ('CONNECT', (1, 'C', 'left')),
    ('OK', (2, 'OK', 'left')),
    ('PV_TOTAL', (5, 'PVS', 'left')),
    ('STATE', (40, 'STATE', 'left')),
    ('MANAGER', (20, 'MANAGER', 'left')),
])

############################################################

class NodeAttr(urwid.WidgetWrap):
    def __init__(self, name, data):
        self.name = name
        self.width = data[0]
        self.align = data[2]

        self.color_stack = ['node attr']

        self.tw = urwid.Text('-', align=self.align)
        self.wrap = urwid.AttrWrap(self.tw, 'node attr')

    def __call__(self):
        return ('fixed', self.width, self.wrap)

    def set_text(self, text):
        self.tw.set_text(text)

    def put_color(self, color):
        try:
            self.color_stack.remove(color)
        except:
            pass
        self.color_stack.append(color)
        self.wrap.set_attr_map({None: color})
        # logging.info('%s' % ({color: color+' focus'}))
        # self.wrap.set_focus_map({color: color+' focus'})

    def pop_color(self, color):
        try:
            self.color_stack.remove(color)
        except:
            pass
        self.wrap.set_attr_map({None: self.color_stack[-1]})



class NodeItem(urwid.WidgetWrap):

    def callback(self, attr=None, pvname=None, value=None, char_value=None, **kw):
        if value is None:
            return

        logging.info('UPDATE: %s = %s' % (pvname, char_value))

        nr = self.attrs[attr]

        # if attr == 'ERROR':
        #     out = ''
        #     if value == 1:
        #         self.attrs['ERROR'].put_color('indicator red')
        #         self.attrs['NODE'].put_color('error')
        #     else:
        #         self.attrs['ERROR'].put_color('indicator green')
        #         self.attrs['NODE'].pop_color('error')

        # elif attr == 'CONNECT':
        #     out = ''
        #     if value == 1:
        #         self.attrs['CONNECT'].put_color('indicator orange')
        #         self.attrs['NODE'].put_color('connect')
        #     else:
        #         self.attrs['CONNECT'].put_color('indicator green')
        #         self.attrs['NODE'].pop_color('connect')

        # elif attr == 'OK':
        #     out = ''
        #     if value == 1:
        #         self.attrs['STATE'].put_color('state ok')
        #         self.attrs['NODE'].put_color('ok')
        #     else:
        #         self.attrs['STATE'].pop_color('state ok')
        #         self.attrs['NODE'].pop_color('ok')

        # else:
        #     if char_value in ['True']:
        #         out = '*'
        #     elif char_value in ['ERROR']:
        #         out = 'E'
        #     elif char_value in ['False', 'OK']:
        #         out = ''
        #     else:
        #         out = char_value

        if attr in ['STALLED', 'ERROR', 'CONNECT', 'OK']:
            out = str(value)
        else:
            out = char_value

        nr.set_text(out)

        if hasattr(self.ui, 'mainloop'):
            try:
                self.ui.mainloop.draw_screen()
            except:
                pass

    def update_ctrl_vars(self, attr, pv):
        logging.info('CTRL UPDATE: %s' % (pv.pvname))
        pv.get(as_string=True)
        #pv.add_callback(self.callback, index=None, with_ctrlvars=True, attr=attr)
        #pv.run_callbacks()

    def get_connection_callback(self, nattr):
        def connection_callback(pv=None, pvname=None, conn=None, **kw):
            attr = nattr
            #logging.info('CONNECT: %s' % (pv.info))
            if conn:
                #self.mainloop.set_alarm_in()
                #threading.Timer(30, self.update_ctrl_vars, [attr, pv]).start()
                pass
            else:
                logging.info('DISCONNECT: %s' % (pvname))
                self.attrs[nattr].set_text('!')
        return connection_callback
        
    def __init__(self, ui, name, node_width):
        self.ui = ui
        self.name = name

        # get the node EPICS device
        self.dev = epics.Device('%s:GRD-%s_' % (os.getenv('IFO'), name))

        self.attrs = {}
        row = []
        for attr, data in NODE_ATTRS.items():

            nr = NodeAttr(attr, data)
            self.attrs[attr] = nr

            if attr == 'NODE':
                nr.set_text(self.name)

            else:
                pv = self.dev.PV(attr, connect=False,
                                 connection_callback=self.get_connection_callback(attr),
                                 )
                pv.add_callback(self.callback, index=None, with_ctrlvars=True, attr=attr)
                logging.debug('%s: %s, %s' % (name, attr, pv.pvname))

            if nr.width > 0:
                row.append(nr())
                row.append(urwid.Divider())

        w = urwid.AttrMap(urwid.AttrMap(urwid.Columns(row), 'node attr'),
                          None,
                          palette.focus_map
                          )
        self.__super.__init__(w)

    def selectable(self):
        return True

    def keypress(self, size, key):
        return key
        if key in self.keys:
            logging.debug("KEY: %s %s" % (self.__class__.__name__, key))
            cmd = eval("self.%s" % (self.keys[key]))
            cmd()
        else:
            return key

############################################################

class NodeWalker(urwid.ListWalker):
    def __init__(self, ui, nodes):
        self.ui = ui
        self.nodes = nodes
        self.ndocs = len(nodes)
        self.focus = 0
        self.items = {}

    def __getitem__(self, pos):
        if pos < 0:
            raise IndexError
        # if pos not in self.items:
        #     self.items[pos] = self.nodes[pos]
        return self.nodes[pos] #self.items[pos]

    def set_focus(self, focus):
        if focus == -1:
            focus = self.ndocs - 1
        self.focus = focus
        self._modified()

    def next_position(self, pos):
        return pos + 1

    def prev_position(self, pos):
        return pos - 1
        
############################################################

class BufferNodes(urwid.WidgetWrap):

    name = "Node list"

    keys = collections.OrderedDict([
        ('n', "nextEntry"),
        ('down', "nextEntry"),
        ('p', "prevEntry"),
        ('up', "prevEntry"),
        ('space', "pageDown"),
        ('page down', "pageDown"),
        ('page up', "pageUp"),
        ('<', "firstEntry"),
        ('>', "lastEntry"),

        ('enter', "view_main"),
        ('g', "view_graph"),
        ('e', "edit_code"),

        ('r', "node_restart"),
        ('S', "node_stop"),
        ('s', "node_start"),
        
        ('meta n', "yank_node"),
        ('meta p', "yank_prefix"),
        ])

    def __init__(self, ui, args=None):
        self.ui = ui
        self.args = args

        nodes = self.ui.nodes_from_glob(args)

        headlist = []
        for field, data in NODE_ATTRS.items():
            width = data[0]
            if width > 0:
                headlist.append(('fixed', width, urwid.Text(data[1], align=data[2])))
                headlist.append(urwid.Divider())
        header = urwid.AttrMap(urwid.Columns(headlist), 'header')

        nw = NodeWalker(ui, nodes)
        #print nw[0]
        self.listbox = urwid.ListBox(nw)

        w = urwid.Frame(self.listbox, header=header)
        self.__super.__init__(w)

    def selectable(self):
        return True

    def keypress(self, size, key):
        #self.ui.set_status()
        if key in self.keys:
            logging.debug("KEY: %s %s" % (self.__class__.__name__, key))
            cmd = eval("self.%s" % (self.keys[key]))
            cmd(size, key)
            return True
        else:
            return key

    def mouse_event(self, size, event, button, col, row, focus):
        self.ui.set_status("%s %s %s %s %s" % (event.upper(), button, col, row, focus))
        if button == 4: # scroll up
            self.prevEntry(size, event)
        elif button == 5: # scroll down
            self.nextEntry(size, event)
        else:
            self.listbox.mouse_event(size, event, button, col, row, focus)

    ####################

    def nextEntry(self, size, key):
        """next node"""
        self.listbox.keypress(size, 'down')

    def prevEntry(self, size, key):
        """previous node"""
        self.listbox.keypress(size, 'up')

    def pageDown(self, size, key):
        """page down"""
        self.listbox.keypress(size, 'page down')

    def pageUp(self, size, key):
        """page up"""
        self.listbox.keypress(size, 'page up')

    def lastEntry(self, size, key):
        """last node"""
        self.listbox.set_focus(-1)

    def firstEntry(self, size, key):
        """first node"""
        self.listbox.set_focus(0)

    ####################

    def get_node(func):
        @functools.wraps(func)
        def inner(self, size, key, **kwargs):
            entry, pos = self.listbox.get_focus()
            return func(self, entry)
        return inner
    
    @get_node
    def view_main(self, node):
        """open main MEDM control screen"""
        util.background_cmd(node.ui, ['guardmedm', node.name])

    @get_node
    def view_graph(self, node):
        """display node graph"""
        util.background_cmd(node.ui, ['guardutil', 'graph', node.name])

    @get_node
    def edit_code(self, node):
        util.background_cmd(node.ui, ['guardutil', 'edit', node.name])

    @get_node
    def node_restart(self, node):
        pass

    @get_node
    def node_stop(self, node):
        pass

    @get_node
    def node_start(self, node):
        pass

    @get_node
    def yank_node(self, node):
        """copy node name to clipboard"""
        name = node.name
        util.xclip(name)
        self.ui.set_status('yanked node name: %s' % name)

    @get_node
    def yank_prefix(self, node):
        """copy node CA control prefix to clipboard"""
        prefix = node.dev._prefix
        util.xclip(prefix)
        self.ui.set_status('yanked node control prefix: %s' % prefix)
