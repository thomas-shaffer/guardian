# much of the structure here was cribbed from
# https://github.com/pypa/sampleproject

import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the README file
with open(os.path.join(here, 'README.md')) as f: #, encoding='utf-8') as f:
    long_description = f.read()

version = {}
with open("lib/guardian/_version.py") as f:
    exec(f.read(), version)

setup(
    name = 'guardian',
    version = version['__version__'],
    description = 'aLIGO Guardian',
    long_description = long_description,
    author = 'Jameson Graef Rollins',
    author_email = 'jameson.rollins@ligo.org',
    url = 'https://git.ligo.org/cds/guardian.git',
    license = 'GPLv3+',
    keywords = [],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Environment :: Console',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
    ],

    # install_requires = [
    #     'ezca',
    #     'ipython',
    #     'networkx',
    #     'pcaspy',
    #     'pydotplus',
    #     'setproctitle',
    #     ],

    package_dir = {'': 'lib'},
    packages = [
        'guardian',
        'guardian.medm',
        'guardian.medm.screens',
        'guardctrl',
    ],
    # https://chriswarrick.com/blog/2014/09/15/python-apps-the-right-way-entry_points-and-scripts/
    # should we have a 'gui_scripts' as well?
    # entry_points={
    #     'console_scripts': [
    #         'guardian = guardian.__main__:main',
    #         'guardmedm = guardian.medm.__main__:main',
    #         'guardutil = guardian.guardutil:main',
    #         'guardctrl = guardctrl.__main__:main',
    #     ],
    # },
    scripts = [
        'bin/guardian',
        'bin/guardmedm',
        'bin/guardutil',
        'bin/guardctrl',
        'bin/guardlog',
        ],

    # FIXME: this is a hack.  this should be in data_files, but
    # there's no good way to access the data_files install location
    # after install: https://bbs.archlinux.org/viewtopic.php?id=98017
    package_data = {'guardian.medm.screens': ['*.adl']}
    # data_files = [
    #     ('share/guardian', ['share/GUARD.adl']),
    #     ]

)
